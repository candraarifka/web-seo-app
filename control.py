from flask import Flask, render_template, url_for, request, redirect, send_file
import requests
from bs4 import BeautifulSoup
import random
import re
import sqlite3 as sql
from flask.ext.uploads import UploadSet, configure_uploads, IMAGES
from basicseo import basic
from textanalyzer import analyzer
from sitemaptest import mapTest


app = Flask(__name__)
photos=UploadSet('photos', IMAGES)
app.config['UPLOADED_PHOTOS_DEST']='static/img'
configure_uploads(app,photos)
app.secret_key='rahasia'


@app.route('/')
def home():
	return render_template('home.html')

@app.route('/basicseo', methods=["POST","GET"])
def basicseo():
	hasil = request.form.get("myUrl")

	with sql.connect("database.db") as con:
		cur = con.cursor()

		cur.execute("""INSERT INTO tesdb (web_name)
			VALUES (?)""",(hasil,) )

		con.commit()
		#con.close()

	if hasil == None:
		jmlh=["Masukkan situs"]

	elif hasil != "":
		if hasil != None:
			tes = basic(hasil)
			jmlh=tes
	else:
		jmlh=["Masukkan alamat web!!"]

	return render_template('basicseo.html', jmlh=jmlh )


@app.route('/textanalyzer', methods=['POST','GET'])
def textanalyzer():
	judul=request.form.get('judul')
	keyword=request.form.get('keyword')
	artikel=request.form.get('artikel')

	if judul and keyword and artikel != None:
		cek = analyzer(judul, keyword, artikel)
		semua=cek
	else:
		semua=["Lengkapi semua form!"]

	return render_template('textanalyzer.html', semua=semua)


@app.route('/sitemaptest', methods=['POST','GET'])
def sitemaptest():

	sitemap1=request.form.get('map')

	if sitemap1 == None:
		hasilNya=["Masukkan situs"]

	elif sitemap1 != "":
		cek=mapTest(sitemap1)
		hasilNya=cek

	else:
		hasilNya=["Masukkan situs"]

	return render_template("sitemaptest.html", hasilNya=hasilNya)


@app.route('/return-file')
def return_file():
	return send_file('../static/your_sitemap.txt',attachment_filename='your_sitemap.txt')


@app.route('/article-generator', methods=['GET','POST'])
def artikelgenerator():
	artikel1=request.form.get('artikel1')

	if artikel1 != "":
		def multiwordReplace(text, wordDic):
			rc = re.compile('|'.join(map(re.escape, wordDic)))
			def translate(match):
				return wordDic[match.group(0)]

			ini=rc.sub(translate, text)
			return ini

		# bank kata untuk diganti
		wordDic = {
		'aku': 'saya',
		'kamu': 'anda',
		'dapat': 'bisa',
		'cinta': 'sayang',
		'memiliki': 'mempunyai'}

		# panggil fungsi dan ganti text
		str1=str(artikel1)
		str2 = multiwordReplace(str1, wordDic)

		#print(str2)
		outPut=str2
	else:
		outPut="Masukkan artikel!"

	return render_template('artikelgenerator.html',outPut=outPut)


@app.route('/tessession')
def sess():
	session['user']="Cancan"
	return '<h2>Tes lagi</h2>'

@app.route('/getsession')
def getsess():
	if 'user' in session:
		return session['user']

	return 'Your are not loggin'


@app.route('/tentang')
def tentang():
	return render_template('kontak.html')


@app.route('/kontak')
def kontak():
	return render_template('kontak.html')


@app.errorhandler(404)
def page_note_found(e):
	return render_template('404-file.html')

if __name__ == "__main__":
	app.run(debug=True)

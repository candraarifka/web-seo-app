import requests
from bs4 import BeautifulSoup
import random
import re

def mapTest(sitemap1):
    ua = ["Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19","Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36","Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"]
    headers={"User-Agent":random.choice(ua)}
    url1=sitemap1+"sitemap.xml"
    r=requests.get(url1,headers=headers)
    if r.status_code == 200:
        soup=BeautifulSoup(r.content,"html.parser")
    	urls = [element.text for element in soup.findAll('loc')]

    	def extract_links(url):
    		page = requests.get(url)
    		soup = BeautifulSoup(page.content, 'html.parser')
    		links = [element.text for element in soup.findAll('loc')]
    		return links

    	sitemap_urls = []
    	for url in urls:
            links = extract_links(url)
            sitemap_urls += links

        with open('/home/web-seo-app/static/your_sitemap.txt', 'w') as f:
    		for url in sitemap_urls:
    			f.write(url + '\n')

    	hasilNya=['Ditemukan {:,} URLs dalam sitemap'.format(len(sitemap_urls)), ]

    else:
    	hasilNya=["Tidak ada (<Response [200]>),'Situs tidak memiliki sitemap!'"]

    return hasilNya

import requests
from bs4 import BeautifulSoup
import random
import re

def basic(hasil):
    ua = ["Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"]
    headers={"User-Agent":random.choice(ua)}
    r=requests.get(hasil,headers=headers)
    soup=BeautifulSoup(r.text,"html.parser")
    hSatu=soup.find_all("h1")
    hDua=soup.find_all("h2")
    urlNya=soup.find_all("a")
    meta=soup.find("meta",attrs={'name':'keywords'})
    img=soup.find_all('img')

    #metadescription
    if meta != None:
        metak="Ditemukan"
    else:
    	metak="Tidak ditemukan"

    meta1=soup.find("meta",attrs={'name':'description'})
    if meta1 != None:
    	metad="Ditemukan"
    else:
    	metad="Tidak ditemukan"

    #googleanalyticortagmanager
    ssoup=str(soup)
    analytic=re.findall(r'googletagmanager',ssoup)
    if len(analytic) == 0:
    	hasil="Tidak Ditemukan"
    else:
    	hasil="Ditemukan"

    #imgalt
    img1=re.findall(r'alt=""',str(img))

    metak=metak
    metad=metad
    jmlh1=len(hSatu)
    h23=len(hDua)
    url3=len(urlNya)
    img2=len(img1)

    jmlh2=["H1 yang ditemukan sebanyak : "+ str(jmlh1) +".", "H2 yang ditemukan sebanyak : "+str(h23)+".", "Link yang ditemukan sebanyak : "+str(url3)+".","Meta Keyword "+metak+".","Meta Description "+metad+".", "Google Analytic "+hasil, "Image tanpa keterangan pada alt sebanyak: "+str(img2)]
    jmlh=jmlh2

    return jmlh

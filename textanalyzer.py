import re

def analyzer(judul, keyword, artikel):
    #wordcountarticle
    count=re.findall(r'\w+',artikel)
    count1=len(count)

    #countkeywordinarticle
    keyword1=keyword.lower()
    artikel1=artikel.lower()
    keywrd=re.findall(keyword1,artikel1)
    count2=len(keywrd)

    #keywordin100firstcharacterarticle
    first=artikel[0:110]
    first1=keyword.lower()
    artikel1=artikel.lower()

    if first1 in artikel1:
        count3="Ditemukan"
    else:
        count3="Tidak Ditemukan"

    #keywordintitle
    judul1=judul.lower()
    keyword1=keyword.lower()
    if keyword1 in judul1:
        count4="Ditemukan"
    else:
        count4="Tidak Ditemukan"

    semua=["Jumlah Seluruh Kata Adalah "+str(count1),"Jumlah Keyword Adalah "+str(count2),"Keyword "+count3+" Pada 100 Karakter Pertama.", "Keyword "+count4+" Pada Judul."]

    return semua
